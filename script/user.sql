-- Table: public."user"

-- DROP TABLE public."user";

CREATE TABLE public."user"
(
    id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    age smallint NOT NULL,
    nick_name character varying(64) COLLATE pg_catalog."default",
    create_time timestamp without time zone DEFAULT now(),
    CONSTRAINT user_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public."user"
    OWNER to postgres;