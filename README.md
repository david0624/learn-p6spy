## p6spy是一个很好sql记录工具,简单而且可以还原sql,能给开发带来极大便利

本项目是p6spy的简单配置,配合blog,你将快速上手p6spy

### pom清单

1. slf4j
2. log4j2
3. beetlsql
4. p6spy
5. HikariCP

### 特点

2017-05-05 22:49:46,606 ma I # took 59ms | statement | connection 0|insert into public.user
(name,age,nick_name) VALUES (?,?,?)
insert into public.user
(name,age,nick_name) VALUES ('lijiatu',24,'david');  
可以完整的还原SQL,方便调试,是传统的sql log所不具备的