package pers.apricot;

import com.p6spy.engine.spy.appender.MessageFormattingStrategy;

public class MyStyle implements MessageFormattingStrategy {
	private static final ThreadLocal<StringBuilder> tl = new ThreadLocal<>();

	@Override
	public String formatMessage(int connectionId, String now, long elapsed, String category, String prepared, String sql) {
		StringBuilder sb = tl.get();
		if (null == sb) {
			sb = new StringBuilder(512);
			tl.set(sb);
		}
		sb.delete(0, sb.length());
		sb.append("# took ").append(elapsed).append("ms | ").append(category).append(" | connection ").append(connectionId)
				.append("|").append(prepared);
		if (null == sql || 0 < sql.length()) {
			sb.append('\n').append(sql).append(';');
		}
		return sb.toString();
	}
}
