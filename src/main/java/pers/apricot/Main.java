package pers.apricot;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.beetl.sql.core.*;
import org.beetl.sql.core.db.PostgresStyle;

public class Main {
	private final static HikariDataSource dataSource;

	static {
		String path = "src/main/resources";
		HikariConfig config = new HikariConfig(path + "/HikariCP.props");
		dataSource = new HikariDataSource(config);
	}

	public static void main(String[] args) {
		ConnectionSource single = ConnectionSourceHelper.getSingle(dataSource);
		PostgresStyle postgresStyle = new PostgresStyle();
		ClasspathLoader sqlLoader = new ClasspathLoader("/sql");
		UnderlinedNameConversion underlinedNC = new UnderlinedNameConversion();
		SQLManager sqlManager = new SQLManager(postgresStyle, sqlLoader, single, underlinedNC);
		User user = new User();
		user.setName("lijiatu");
		user.setAge(24);
		user.setNickName("david");
		sqlManager.insert(user);
	}
}
